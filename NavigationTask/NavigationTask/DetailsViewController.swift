//
//  DetailsViewController.swift
//  NavigationTask
//
//  Created by Exi on 17.02.2021.
//

import UIKit

class DetailsViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor.white

        let homeButton = UIButton()
        homeButton.setTitleColor(UIColor.black, for: .normal)
        homeButton.setTitle("Home", for: .normal)
        homeButton.addTarget(self, action: #selector(homeButtonTapped), for: .touchUpInside)

        self.view.addSubview(homeButton)
        
        homeButton.translatesAutoresizingMaskIntoConstraints = false
        homeButton.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        homeButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true

        
        let detailsButton = UIButton()
        detailsButton.setTitleColor(UIColor.black, for: .normal)
        detailsButton.setTitle("Details", for: .normal)
        detailsButton.addTarget(self, action: #selector(detailsButtonTapped), for: .touchUpInside)

        self.view.addSubview(detailsButton)
        
        detailsButton.translatesAutoresizingMaskIntoConstraints = false
        detailsButton.bottomAnchor.constraint(equalTo: homeButton.bottomAnchor, constant: 50).isActive = true
        detailsButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true


    }
    
    @objc func homeButtonTapped() {
        navigationController?.popToRootViewController(animated: true)
    }
    
    @objc func detailsButtonTapped() {
        let vc = ModalViewController()
        vc.modalTransitionStyle = .flipHorizontal
        vc.modalPresentationStyle = .automatic
        present(vc, animated: true, completion: nil)
    }
    
    
    
    
    
}
