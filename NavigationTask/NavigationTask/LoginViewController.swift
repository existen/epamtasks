//
//  ViewController.swift
//  NavigationTask
//
//  Created by Exi on 17.02.2021.
//

import UIKit

class LoginViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        let loginButton = UIButton()
        loginButton.setTitleColor(UIColor.black, for: .normal)
        loginButton.setTitle("Login", for: .normal)
        loginButton.addTarget(self, action: #selector(loginButtonAction), for: .touchUpInside)

        self.view.addSubview(loginButton)
        
        loginButton.translatesAutoresizingMaskIntoConstraints = false
        loginButton.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        loginButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true

        
    }
    
    @objc func loginButtonAction(sender: UIButton!) {
        view.window?.rootViewController = TabBarController()
    }


}

