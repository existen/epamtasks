//
//  HomeViewController.swift
//  NavigationTask
//
//  Created by Exi on 17.02.2021.
//

import UIKit

class HomeViewController: UIViewController {

    init(isLastHomeController: Bool) {
        self.isLastHomeController = isLastHomeController
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        isLastHomeController = true
        super.init(coder: coder)
    }
    
    let isLastHomeController: Bool
    let nextButton = UIButton()

    override func viewDidLoad() {
        
        
        super.viewDidLoad()
        
        view.backgroundColor = UIColor.white
        nextButton.setTitleColor(UIColor.black, for: .normal)
        nextButton.setTitle("Next", for: .normal)
        if isLastHomeController {
            nextButton.addTarget(self, action: #selector(lastViewControllerTapped), for: .touchUpInside)
        } else {
            nextButton.addTarget(self, action: #selector(nextViewController), for: .touchUpInside)
        }

        view.addSubview(nextButton)

        nextButton.translatesAutoresizingMaskIntoConstraints = false

        nextButton.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        nextButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true

        
    }
    
    
    @objc func nextViewController() {
        
        navigationController?.pushViewController(HomeViewController(isLastHomeController: true), animated: true)
        
    }
    
    @objc func lastViewControllerTapped() {
        navigationController?.pushViewController(DetailsViewController(), animated: true)
    }
}
