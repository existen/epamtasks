//
//  TabBarController.swift
//  NavigationTask
//
//  Created by Exi on 17.02.2021.
//

import UIKit

class TabBarController: UITabBarController {
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor.white
        
        let navigation = UINavigationController(rootViewController: HomeViewController(isLastHomeController: false))
        navigation.tabBarItem = UITabBarItem(title: "Home",
                                             image: UIImage(systemName: "tray"),
                                             selectedImage: UIImage(systemName: "tray"))
        
        
        let vc = LogoutViewController()
        vc.tabBarItem = UITabBarItem(title: "Log Out",
                                     image: UIImage(systemName: "arrowshape.turn.up.left"),
                                     selectedImage: UIImage(systemName: "arrowshape.turn.up.left"))
        
        
        setViewControllers([navigation, vc], animated: true)
        
    }
}
