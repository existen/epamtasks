//
//  ModalViewController.swift
//  NavigationTask
//
//  Created by Exi on 17.02.2021.
//

import UIKit

class ModalViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor.white
        
        let detailsLabel = UILabel()
        detailsLabel.textColor = UIColor.black
        detailsLabel.text = "Some details ..."

        detailsLabel.frame = CGRect(x: 140, y: 300, width: 150, height: 15)
        view.addSubview(detailsLabel)

    }
    
    
}
